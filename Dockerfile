FROM python:3.11.4-alpine3.18

LABEL "MAINTAINER"="Karan Lala"
LABEL version="1.0.0"

WORKDIR /usr/src/app

RUN pip install virtualenv
RUN pip install pyyaml==5.3.1

#ADD aws-elastic-beanstalk-cli-setup ./aws-eb-cli-setup

RUN pip install awsebcli --upgrade

#RUN python aws-eb-cli-setup/scripts/ebcli_installer.py

ENTRYPOINT [ "eb" ]

CMD [ "--help" ]